#!/bin/sh
set -e
autoreconf --install
perl -i.bak -pe '$_="" if m/^(?:Makefile|config\.h):/..m/^$/' \
	Makefile.in
